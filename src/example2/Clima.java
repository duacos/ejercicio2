
package example2;

import java.time.Instant;
import java.util.Date;

public class Clima {
    
    private String ciudad;
    private Date fechaActual;
    private float grados;
    private float probabilidadPresipitaciones;
    private float nivelesDehumedad;
    private double velocidadDeViento;

    public Clima(
            String ciudad, 
            float grados, 
            float probabilidadPresipitaciones, 
            float nivelesDehumedad, 
            double velocidadDeViento) {
        
        this.ciudad = ciudad;
        this.fechaActual = Date.from(Instant.now());
        this.setGrados(grados);
        this.setProbabilidadPresipitaciones(probabilidadPresipitaciones);
        this.setNivelesDehumedad(nivelesDehumedad);
        this.setVelocidadDeViento(velocidadDeViento);
    }
    
    
    public float celsius_fahrenheit() {
        float fahrenheit = ((this.grados * 9)/5) + 32;
        return fahrenheit;
    }
    
    public void mostrarDatosDeClima() {
        System.out.println(""
                + "Ciudad: " +  this.ciudad + "\n"
                + "Grados: " +  this.grados + " °C o " + this.celsius_fahrenheit() + " °F \n"
                + "Fecha Actual: " +  this.fechaActual + "\n"
                + "probabilidad Presipitación: " +  this.probabilidadPresipitaciones + "\n"
                + "niveles de humedad: " +  this.nivelesDehumedad + "\n"
                + "viento: " +  this.velocidadDeViento
        
        );
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Date getFechaActual() {
        return fechaActual;
    }

    public void setFechaActual(Date fechaActual) {
        this.fechaActual = fechaActual;
    }

    public float getGrados() {
        return grados;
    }

    public void setGrados(float grados) {
        if(grados > 0) 
            this.grados = grados;  
        else
            System.out.println("Los grados deben ser positivos");
        
    }

    public float getProbabilidadPresipitaciones() {
        
        return probabilidadPresipitaciones;
    }

    public void setProbabilidadPresipitaciones(float probabilidadPresipitaciones) {
        if(grados > 0) 
            this.probabilidadPresipitaciones = probabilidadPresipitaciones;
        else
            System.out.println("La probabilidad de presipitación debe ser positiva");
    }

    public float getNivelesDehumedad() {
        return nivelesDehumedad;
    }

    public void setNivelesDehumedad(float nivelesDehumedad) {
        if(grados > 0)
            this.nivelesDehumedad = nivelesDehumedad;
        else
            System.out.println("Los niveles de humedad deben ser positivos");
    }

    public double getVelocidadDeViento() {
        return velocidadDeViento;
    }

    public void setVelocidadDeViento(double velocidadDeViento) {
        if(grados > 0)
            this.velocidadDeViento = velocidadDeViento;
        else
            System.out.println("la velocidad del viento debe ser positiva");
    }
}
