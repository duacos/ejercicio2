
package example2;

import java.time.Instant;
import java.util.Date;

/**
 *
 * @author duacos
 */
public class Example2 {


    public static void main(String[] args) {
        
        Clima clima = new Clima("Cartagena", 28, 50, 40, 150);
        System.out.println("------------------------------");
        clima.mostrarDatosDeClima();
    }
    
}
